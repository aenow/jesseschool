$(function () {
    run();

    function run() {
        bind();
    }

    function bind() {
        $('#add63254').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'dashboard.php',
                data: $(this).serialize(),
                success: function () {
                   // location.reload();
                }
            });
        });

        $('#location9848932').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'dashboard.php',
                data: $(this).serialize(),
                success: function () {
                    location.reload();
                }
            });
        });

        $('#login988394').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'index.php',
                data: $(this).serialize(),
                success: function (success) {
                    location.reload();
                }
            });
        });

        $('.full-book-listing').css('display', 'block');
        $('.single-book-listing').css('display', 'none');

        $('#search659954').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'dashboard.php',
                data: $(this).serialize(),
                success: function () {
                    $('.full-book-listing').css('display', 'none');
                    $('.single-book-listing').css('display', 'block');
                }
            });
        });

        $('#check89156').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: 'dashboard.php',
                data: $(this).serialize(),
                success: function () {
                    location.reload();
                }
            });
        });

        $('.checkout-button').on('click', function (e) {
            e.preventDefault();

            $dataObj = (
                {
                    'RemoveBook': false,
                    'CheckoutBook': true,
                    'BookID': $(this).siblings('.book-id-column').text(),
                    'Book_Name': $(this).siblings('.book-title-column').text(),
                    'Book_Type': $(this).siblings('.book-type-column').text(),
                    'Book_Copies': $(this).siblings('.book-copies-column').text(),
                    'Book_Location': $(this).siblings('.book-location-column').text()
                }
            );

            $.ajax({
                type: 'POST',
                url: 'dashboard.php',
                data: $dataObj,
                success: function () {
                    location.reload();
                }
            });
        });

        $('.remove-button').on('click', function (e) {
            e.preventDefault();

            $dataObj = (
                {
                    'RemoveBook': true,
                    'CheckoutBook': false,
                    'BookID': $(this).siblings('.book-id-column').text(),
                    'Book_Name': $(this).siblings('.book-title-column').text(),
                    'Book_Type': $(this).siblings('.book-type-column').text(),
                    'Book_Copies': $(this).siblings('.book-copies-column').text(),
                    'Book_Location': $(this).siblings('.book-location-column').text()
                }
            );

            $.ajax({
                type: 'POST',
                url: 'dashboard.php',
                data: $dataObj,
                success: function () {
                    location.reload();
                }
            });
        });
    }
});