<?php
require('vendor/autoload.php');
include 'php-functions/db-functions.php';

session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/form.js"></script>
</head>
<body>

<div class="full-website-wrapper">
    <div class="title-area">
        <h1>Cascade Isle District Libraries</h1>
    </div>
    <div class="menu-area">
        <a href="/">Home</a> <span>|</span> <a href="locations.php">Locations</a>
        <?php
            // checks to see if a employee or patron is logged in and updates the menu accordingly
            if (isset($_SESSION['employee'])) {
                if($_SESSION['employee'] == '1'){
                    echo '<span>|</span> <a href="dashboard.php">Dashboard</a>';
                } else {
                    echo '<span>|</span> <a href="catalog.php">Catalog</a>';
                }
            }
        ?>
    </div>
    <div class="content-area">
        <section class="book-catalog">
            <div class="book-info">
                <span class="book-id-column"><u>ID</u></span>
                <span class="book-title-column"><u>Name</u></span>
                <span class="book-type-column"><u>Type</u></span>
                <span class="book-copies-column"><u>Available Copies</u></span>
                <span class="book-location-column"><u>Location</u></span>
            </div>
            <br>
            <?php
            // Selects all the books from the book table and displays them on the home page
            $db = retrieve();
            $books = array_merge($db->getAll("SELECT * FROM book"));

            foreach ($books as $book) {
                echo '<div class="book-info">';
                echo '<span class="book-id-column">' . $book["BookID"] . '</span>';
                echo '<span class="book-title-column">' . $book["Book_Name"] . '</span>';
                echo '<span class="book-type-column">' . $book["Book_Type"] . '</span>';
                echo '<span class="book-copies-column">' . $book["Book_Copies"] . '</span>';
                echo '<span class="book-location-column">' . $book["Book_Location"] . '</span>';
                echo '</div>';
            }
            ?>
        </section>
        <?php
        if (isset($_POST['user_name'])) {
            $user_name = $_POST['user_name'];
            $password = $_POST['password'];

            $patron = $db->execute("
                SELECT Username, Password, First_Name
                FROM patron
                WHERE Username = ?
                ",
                    array(
                        $user_name
                    )
            );

            $employee = $db->execute("
                SELECT EmployeeID, Password, First_Name
                FROM employee
                WHERE EmployeeID = ?
                ",
                array(
                    $user_name
                )
            );

            if ($patron->fields){
                $_SESSION['login'] = true;
                $_SESSION['first_name'] = $patron->fields["First_Name"];
                $_SESSION['username'] = $user_name;
                $_SESSION['employee'] = 0;
            } else if ($employee->fields){
                $_SESSION['login'] = true;
                $_SESSION['first_name'] = $employee->fields["First_Name"];
                $_SESSION['employee'] = 1;
            }
        }

        ?>
        <?php if (!isset($_SESSION['login'])) { ?>
            <aside class="login-area">
                <div>
                    <span class="library-id-column"><u> Login</u></span>
                </div>
                <br>
                <form id="login988394" method="post">
                    Username:<br>
                    <input type="text" name="user_name"><br>
                    Password:<br>
                    <input type="text" name="password">
                    <br>
                    <input type="submit" class="login988394" value="Submit">
                </form>
            </aside>
        <?php } else { ?>
            <aside class="login-area">
                <?php echo 'Welcome ' . $_SESSION['first_name']; ?>
            </aside>
        <?php } ?>
    </div>
</div>

</body>
</html>
