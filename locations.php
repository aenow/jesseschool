<?php
require('vendor/autoload.php');
include 'php-functions/db-functions.php';

// Starts session for all logged in users on the page
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
</head>
<body>

<div class="full-website-wrapper">
    <div class="title-area">
        <h1>Cascade Isle District Libraries</h1>
    </div>
    <div class="menu-area">
        <a href="/">Home</a> <span>|</span> <a href="locations.php">Locations</a>
        <?php
        // Checks to see if a user is logged in and displays menu links accordingly
        if (isset($_SESSION['employee'])) {
            if($_SESSION['employee'] == '1'){
                echo '<span>|</span> <a href="dashboard.php">Dashboard</a>';
            } else {
                echo '<span>|</span> <a href="catalog.php">Catalog</a>';
            }
        }
        ?>
    </div>
    <div class="content-area">
        <section class="library-area">
            <div class="library-info">
                <span class="library-id-column"><u>ID</u></span>
                <span class="library-title-column"><u>Name</u></span>
                <span class="library-address-column"><u>Street Address</u></span>
                <span class="library-zipcode-column"><u>Zip code</u></span>
            </div><br>
            <?php
            // Retrieves all variables from the library database
            $db = retrieve();
            $locations = array_merge($db->getAll("SELECT * FROM library"));

            foreach($locations as $location) {
                echo '<div class="library-info">';
                echo '<span class="library-id-column">' . $location["LibraryId"] . '</span>';
                echo '<span class="library-title-column">' . $location["Library_Name"] . '</span>';
                echo '<span class="library-address-column">' . $location["Street_Address"] . '</span>';
                echo '<span class="library-zipcode-column">' . $location["Zip_code"] . '</span>';
                echo '</div>';
            }
            ?>
        </section>
        <aside class="login-area">
            <?php if (isset($_SESSION['login'])) { ?>
                // Displays message if user is logged in
                <aside class="login-area">
                    <?php echo 'Welcome ' . $_SESSION['first_name']; ?>
                </aside>
            <?php } ?>
        </aside>
    </div>
</div>

</body>
</html>
