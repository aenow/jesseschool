<?php
require('vendor/autoload.php');

$dbhost = "152.178.1.68";
$dbuser = "JesseSchool";
$dbpass = 'testtest';
$dbname = "JesseSchool";
$driver = 'mysqli';
$db = adoNewConnection($driver);
$db->connect($dbhost, $dbuser, $dbpass, $dbname);


if ($_POST) {
    if (isset($_POST["RemoveBook"]) || isset($_POST["CheckoutBook"])) {
        // Remove a Book from database

        // A employee must enter the Book ID of which they would like to remove from the database
        // This is done on the employee dashboard. Once this is done then the employee and patron
        // lists are updated to show the new book listing on their dashboard/catalog.

        if ($_POST["RemoveBook"] == 'true') {
            $BookID = $_POST["BookID"];

            $db->execute("
            DELETE FROM book
            WHERE BookID = ?
            ",
                array(
                    $BookID
                )
            );

        } else {
            // Check a book out

            // A employee must enter the Book ID of which they would like to checkout
            // This is done on the employee dashboard or patron's catalog. Once this is done then the employee and patron
            // lists are updated to show the correct amount of the copies of the book left

            $BookID = $_POST["BookID"];

            $db->execute("
            UPDATE book
            SET Book_Copies = Book_Copies - 1
            WHERE BookID = ?
            ",
                array(
                    $BookID
                )
            );
        }
    }

    if (isset($_POST["location_book_id"]) && !empty($_POST["location_book_id"])) {
        $BookID = $_POST["location_book_id"];
        $book_old_location = $_POST["book_old_location"];
        $book_new_location = $_POST["book_new_location"];

        // This changes a books location from the old one to the new one

        /* Employee will need to enter a book's id, current location, and the new location the book is being
        transferred to on the employee's dashboard. Once the info is entered and submitted the patron's catalog and
        employee book listing is updated  */

        $db->execute("
            UPDATE book
            SET Book_Location = ?
            WHERE BookID = ? 
            AND Book_Location = ?
            ",
            array(
                $book_new_location,
                $BookID,
                $book_old_location
            )
        );
    }

    if (isset($_POST["add_bookname"]) && !empty($_POST["add_bookname"])) {
        $Book_Name = $_POST["add_bookname"];
        $Book_Type = $_POST["add_booktype"];
        $Book_Location = $_POST["add_booklocation"];
        $Book_Copies = $_POST["add_bookcopies"];

        // This allows a employee to add a book to the database. They will need to provide the name, location,
        // type, and number of copies the book has. Once this is addition is submitted the database will be updated
        // with the new book included.

        $db->execute(
            "
              INSERT INTO book (Book_Name, Book_Location, Book_Type, Book_Copies) 
              VALUES (?, ?, ?, ?)
            ",
            array(
                $Book_Name,
                $Book_Location,
                $Book_Type,
                $Book_Copies
            )
        );
    }

    if (isset($_POST["check_in_id"]) && !empty($_POST["check_in_id"])) {
        $BookID = $_POST["check_in_id"];

        /*
            This allows the ability to check a book in. A employee must enter the book's id from the book listing.
            Once the book id is provided the database updates the number of copies by incrementing them
        */

        $db->execute("
            UPDATE book
            SET Book_Copies = Book_Copies + 1
            WHERE BookID = ?
            ",
            array(
                $BookID
            )
        );
    }
}