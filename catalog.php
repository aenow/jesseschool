<?php
require('vendor/autoload.php');
include 'php-functions/db-functions.php';
include 'php-functions/form.php';

session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/form.js"></script>
</head>
<body>

<div class="full-website-wrapper">
    <div class="title-area">
        <h1>Cascade Isle District Libraries</h1>
    </div>
    <div class="menu-area">
        <a href="/">Home</a> <span>|</span> <a href="locations.php">Locations</a>
        <?php
        if (isset($_SESSION['employee'])) {
            if($_SESSION['employee'] == '1'){
                echo '<span>|</span> <a href="dashboard.php">Dashboard</a>';
            } else {
                echo '<span>|</span> <a href="catalog.php">Catalog</a>';
            }
        }
        ?>
    </div>
    <div class="title-area" style="margin-bottom: 25px">
        <h2>Patron Dashboard</h2>
    </div>
    <div class="content-area">
        <section class="book-catalog employee-dashboard full-book-listing">
            <div class="book-info">
                <span class="book-id-column"><u>ID</u></span>
                <span class="book-title-column"><u>Name</u></span>
                <span class="book-type-column"><u>Type</u></span>
                <span class="book-copies-column"><u>Available Copies</u></span>
                <span class="book-location-column"><u>Location</u></span>
            </div>
            <br>
            <?php
            $db = retrieve();
            $books = array_merge($db->getAll("SELECT * FROM book"));

            foreach ($books as $book) {
                echo '<div class="book-info">';
                echo '<span class="book-id-column">' . $book["BookID"] . '</span>';
                echo '<span class="book-title-column">' . $book["Book_Name"] . '</span>';
                echo '<span class="book-type-column">' . $book["Book_Type"] . '</span>';
                echo '<span class="book-copies-column">' . $book["Book_Copies"] . '</span>';
                echo '<span class="book-location-column">' . $book["Book_Location"] . '</span>';
                echo '<button id="checkout-id-' . $book['BookID'] . '" class="checkout-button checkout-id-' . $book['BookID'] . '">Checkout</button>';
                echo '</div>';

            }
            ?>
        </section>
    </div>
</div>

</body>
</html>