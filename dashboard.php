<?php
require('vendor/autoload.php');
include 'php-functions/db-functions.php';
include 'php-functions/form.php';

session_start();
// Only Employees are allowed to see this page. If the user isn't a employee it redirects to main page
if (isset($_SESSION['employee'])) {
    if($_SESSION['employee'] == '0'){
        echo '<script>window.location = "/index.php"</script>';
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/form.js"></script>
</head>
<body>

<div class="full-website-wrapper">
    <div class="title-area">
        <h1>Cascade Isle District Libraries</h1>
    </div>
    <div class="menu-area">
        <a href="/">Home</a> <span>|</span> <a href="locations.php">Locations</a>
        <?php
        if (isset($_SESSION['employee'])) {
            if($_SESSION['employee'] == '1'){
                echo '<span>|</span> <a href="dashboard.php">Dashboard</a>';
            } else {
                echo '<span>|</span> <a href="catalog.php">Catalog</a>';
            }
        }
        ?>
    </div>
    <div class="title-area" style="margin-bottom: 25px">
        <h2>Employee Dashboard</h2>
    </div>
    <div class="content-area">
        <aside class="dashboard-aside">
            <div>
                <span><u>Add Book</u></span>
            </div>
            <form id="add63254" method="post">
                Book Name:<br>
                <input type="text" name="add_bookname">
                <br>
                Book Type:<br>
                <input type="text" name="add_booktype">
                <br>
                Book Location:<br>
                <input type="text" name="add_booklocation">
                <br>
                Book Copies<br>
                <input type="text" name="add_bookcopies">
                <br>
                <input type="submit" class="add63254" value="Submit">
            </form>
        </aside>
        <section class="dashboard-section">
            <div>
                <span><u>Check In Book</u></span>
            </div>
            <form id="check89156" method="post">
                Book ID:<br>
                <input type="text" name="check_in_id">
                <br>
                <input type="submit" class="add63254" value="Submit">
            </form>
        </section>
        <section class="dashboard-section">
            <div>
                <span><u>Change Book Location</u></span>
            </div>
            <form id="location9848932" method="post">
                Book ID:<br>
                <input type="text" name="location_book_id">
                <br>
                Book's current location:<br>
                <input type="text" name="book_old_location">
                <br>
                Book's new location:<br>
                <input type="text" name="book_new_location">
                <br>
                <input type="submit" class="location9848932" value="Submit">
            </form>
        </section>
        <aside class="dashboard-aside">
            <div>
                <span><u>Filter Books</u></span>
            </div>
            <form id="search659954" method="post">
                Book ID:<br>
                <input type="text" name="search_book_id">
                <br>
                <input type="submit" class="search659954" value="Submit">
            </form>
        </aside>
    </div>
    <section class="book-catalog employee-dashboard single-book-listing">
        <div class="book-info">
            <span class="book-id-column"><u>ID</u></span>
            <span class="book-title-column"><u>Name</u></span>
            <span class="book-type-column"><u>Type</u></span>
            <span class="book-copies-column"><u>Available Copies</u></span>
            <span class="book-location-column"><u>Location</u></span>
        </div>
        <br>
        <?php
        // Selects a certain book based on filter criteria

        if(isset($_POST["search_book_id"]) && !empty($_POST["search_book_id"])){
            $BookID = $_POST["search_book_id"];

            $book = $db->execute("
                SELECT *
                FROM book
                WHERE BookID = ? 
                ",
                    array (
                        $BookID
                    )
            )->fields;

            echo '<div class="book-info">';
            echo '<span class="book-id-column">' . $book["BookID"] . '</span>';
            echo '<span class="book-title-column">' . $book["Book_Name"] . '</span>';
            echo '<span class="book-type-column">' . $book["Book_Type"] . '</span>';
            echo '<span class="book-copies-column">' . $book["Book_Copies"] . '</span>';
            echo '<span class="book-location-column">' . $book["Book_Location"] . '</span>';
            echo '<button id="checkout-id-' . $book['BookID'] . '" class="checkout-button checkout-id-' . $book['BookID'] . '">Checkout</button>';
            echo '<button id="remove-id-' . $book['BookID'] . '" class="remove-button remove-id-' . $book['BookID'] . '">Remove</button>';
            echo '</div>';
        }
        ?>
    </section>
    <section class="book-catalog employee-dashboard full-book-listing">
        <div class="book-info">
            <span class="book-id-column"><u>ID</u></span>
            <span class="book-title-column"><u>Name</u></span>
            <span class="book-type-column"><u>Type</u></span>
            <span class="book-copies-column"><u>Available Copies</u></span>
            <span class="book-location-column"><u>Location</u></span>
        </div>
        <br>
        <?php
        // Retrieves all information about books and displays them on the page through a foreach loop
        $db = retrieve();
        $books = array_merge($db->getAll("SELECT * FROM book"));

        foreach ($books as $book) {
            echo '<div class="book-info">';
            echo '<span class="book-id-column">' . $book["BookID"] . '</span>';
            echo '<span class="book-title-column">' . $book["Book_Name"] . '</span>';
            echo '<span class="book-type-column">' . $book["Book_Type"] . '</span>';
            echo '<span class="book-copies-column">' . $book["Book_Copies"] . '</span>';
            echo '<span class="book-location-column">' . $book["Book_Location"] . '</span>';
            echo '<button id="checkout-id-' . $book['BookID'] . '" class="checkout-button checkout-id-' . $book['BookID'] . '">Checkout</button>';
            echo '<button id="remove-id-' . $book['BookID'] . '" class="remove-button remove-id-' . $book['BookID'] . '">Remove</button>';
            echo '</div>';

        }
        ?>
    </section>
</div>

</body>
</html>